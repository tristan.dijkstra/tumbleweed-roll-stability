import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits import mplot3d


class QuaternionRotation:
    '''Generate Quaternion object with rotation angle and rotation axis as inputs (i,j,k). Returns the rotation matrix'''
    def __init__(self, angle, i, j, k):
        self.angle = np.radians(angle/2)
        self.Quaternion = np.array(
            [np.cos(self.angle), i * np.sin(self.angle), j * np.sin(self.angle), k * np.sin(self.angle)])
        self.refresh()

    def refresh(self, newAngle=False):
        '''Generates unit quaternion and rotation matrix from quaternion'''
        self.unitQuaternion = self.Quaternion / np.linalg.norm(self.Quaternion)
        r, i, j, k = self.unitQuaternion[0], self.unitQuaternion[1], self.unitQuaternion[2], self.unitQuaternion[3]
        if newAngle:
            self.angle = np.arccos(r)

        # Formula 2.6 from:
        # https://www.sciencedirect.com/topics/computer-science/quaternion-multiplication
        self.rotationMatrix = np.array(
            [[1 - (2 * (j**2 + k**2)), 2 * (i * j - k * r), 2 * (i * k + j * r)],
             [2 * (i * j + k * r), 1 - (2 * (i**2 + k**2)), 2 * (j * k - i * r)],
             [2 * (i * k - j * r), 2 * (j * k + i * r), 1 - (2 * (i**2 + j**2))]])


    def __repr__(self):
        ''' '''
        # might remove this or change to return unit quaternion
        return self.rotationMatrix

    def __str__(self):
        '''Returns unit Quaternion'''
        return np.array2string(self.unitQuaternion) 

    def rotate(self, coordinate):
        '''Rotate a vector coordinate takes vector to be rotated as input'''
        return np.matmul(self.rotationMatrix, coordinate)
    
    def multiply(self, q2):
        '''Add an aditional compound a rotation by means of quaternion multiplication. Takes q2 as a quaternion vector'''
        xyz1, xyz2 = self.Quaternion[1:4], q2.Quaternion[1:4]   #imaginary values of quaternions
        r1, r2 = self.Quaternion[0], q2.Quaternion[0]           #real value of quaternions
        
        # Formula 2.5 from:
        # https://www.sciencedirect.com/topics/computer-science/quaternion-multiplication
        xyznew = np.cross(xyz2, xyz1) + r2 * xyz1 + r1 * xyz2
        rnew = r2 * r1 - np.dot(xyz2,xyz1)
        
        qnew = [rnew, xyznew[0], xyznew[1], xyznew[2]]
        self.Quaternion = np.array(qnew)
        self.refresh(newAngle=True)

qr = QuaternionRotation #shorthand alias, could rename the class later



class Ring:
    '''Solid hollow flat cylinder (ring), local Z-axis is its rotational symmetry axis'''
    def __init__(self,R_inner, R_outer, height, density, static_quaternion):
        '''Initialize Ring object'''
        # Define inputs as properties of the ring:
        self.unitBasis = np.array([[1,0,0],[0,1,0],[0,0,1]])

        self.r1 = R_inner
        self.r2 = R_outer
        self.h = height
        self.p = density
        
        # Compute Mass:
        V = self.h * np.pi * (self.r2**2 - self.r1**2)
        self.m = V * self.p

        # Rotation using Quaturnions
        self.static_quaternion = static_quaternion
        self.final_quaternion = static_quaternion
        empty_quaternion = QuaternionRotation(0,1,0,0)

        # Run Initial update of the ring without rotating
        self.update(empty_quaternion)

    def update(self, quaternion_update):
        '''Recompute mass, bases, and Inertia Tensor'''
        #Update total quaturnion
        self.final_quaternion.multiply(quaternion_update)
        
        # Define principle axis
        self.i = self.final_quaternion.rotate(self.unitBasis[0]) 
        self.j = self.final_quaternion.rotate(self.unitBasis[1]) 
        self.k = self.final_quaternion.rotate(self.unitBasis[2]) 

        # Compute MMOI around all priciple axis:
        # https://en.wikipedia.org/wiki/List_of_moments_of_inertia. 
        Ixx = 1/12 * self.m * (3*(self.r2**2 + self.r1**2)+self.h**2)
        Iyy = 1/12 * self.m * (3*(self.r2**2 + self.r1**2)+self.h**2)
        Izz = 1/2 * self.m * (self.r2**2 + self.r1**2)

        # Define inertia matrix, where Ixy, Ixz, Iyz are zero due to symmetry:
        I = np.array([[Ixx, 0, 0],
                      [0, Iyy, 0],
                      [0, 0, Izz]])
        
        # Get inertia tensor around axes i,j,k, given the inertia tensor I around principle axis q,r,s
        # source: 
        i,j,k = self.unitBasis 
        q,r,s = self.i, self.j, self.k
        T = np.array([[np.dot(i,q), np.dot(i,r), np.dot(i,s)],
                  [np.dot(j,q), np.dot(j,r), np.dot(j,s)],
                  [np.dot(k,q), np.dot(k,r), np.dot(k,s)]])
    
        self.I = np.matmul(np.matmul(T, I), np.transpose(T))

    def plot(self, ax, plotBasis = True, N=60, color = "grey"):
        '''plot the ring in a 3D graph'''

        angles = np.linspace(0, 2*np.pi, N)
        # basic ring is given on xy axis
        
        ring = []
        for angle in angles:
            ring.append(self.r2 * np.cos(angle) * self.i + self.r2 * np.sin(angle) * self.j)
        
        points = np.array(ring)
        ax.plot3D(*points.T, color)

        if plotBasis:
            origin = np.array([0,0,0])
            ax.quiver(*origin, *self.i, color="r")
            ax.quiver(*origin, *self.j, color="g")
            ax.quiver(*origin, *self.k, color="b")


class BoxSet:
    def getSteiner(self, mass, x, y, z):
        '''Takes mass and carthesian coordinates (of a block) and outputs an array with the three steiner terms'''
        # http://www2.eng.cam.ac.uk/~hemh1/gyroscopes/momentinertia.html
        steiner = mass * np.array([y**2 + z**2, x**2 + z**2, x**2 + y**2])
        return steiner

    def massMOI(self, x, y, z):
        '''Finds Mass Moment of Inertia for all blocks'''
        Iarray = []
        # gets the steiner array for each block using the getSteiner method
        for index in range(self.numberOfBoxes):
            Iarray.append(self.getSteiner(self.masses[index], x[index], y[index], z[index]))
        return np.array(Iarray)

    def __init__(self, radius, numberOfBoxes, boxMasses, quaternion, startAngle = 0):
        '''Ring Class that makes up one of the rings on the tumbleweed'''
        assert numberOfBoxes % 2 == 0, "Attribute numberOfBoxes must be an equal number"

        self.r = radius
        self.numberOfBoxes = numberOfBoxes
        self.masses = np.array(boxMasses)
        self.quaternion = quaternion

        self.unitBasis = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])

        self.i = self.quaternion.rotate(self.unitBasis[0])
        self.j = self.quaternion.rotate(self.unitBasis[1])
        self.k = self.quaternion.rotate(self.unitBasis[2])

        # make a ring of boxes on xy plane
        angles = np.array([((2*np.pi/(self.numberOfBoxes)) * x) + np.degrees(startAngle) for x in range(self.numberOfBoxes)])
        x = np.array([self.r * np.cos(angle) for angle in angles])
        y = np.array([self.r * np.sin(angle) for angle in angles])
        z = np.zeros((self.numberOfBoxes))

        self.coords = np.array([x,y,z]).T #Coords used for plotting
        self.MMOIlist = self.massMOI(x, y, z)

        #this is the total inertia vector. might rename.
        self.Io = np.sum(self.MMOIlist, axis=0) * np.identity(3) 


    def plot(self, ax, plotBasis=False):
        plotPoints = []
        for coord in self.coords:
            plotPoints.append(self.quaternion.rotate(coord))
        plotPoints = np.array(plotPoints).T
        ax.scatter(*plotPoints, s=50)

        if plotBasis:
            origin = np.array([0, 0, 0])
            ax.quiver(*origin, *self.i, color="r")
            ax.quiver(*origin, *self.j, color="g")
            ax.quiver(*origin, *self.k, color="b")

class Tumbleweed:
    def __init__(self, radius, ringConfiguration, thicknessConfiguration, boxConfiguration, initialQuaternion):
        '''Main tumbleweed class, calculates with total properties'''
        self.R = radius
        self.colors = ["k", "r", "r", "g", "g", "b", "b"]
        self.quaternion = initialQuaternion
        self.ringConfiguration = ringConfiguration

        self.unitBasis = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])

        self.i = self.quaternion.rotate(self.unitBasis[0])
        self.j = self.quaternion.rotate(self.unitBasis[1])
        self.k = self.quaternion.rotate(self.unitBasis[2])
        self.basis = np.array([self.i, self.j, self.k])
        

        self.rings = []
        for idx, quaternion in enumerate(ringConfiguration):
            # Assume square arc cross-section for rings
            t = thicknessConfiguration[idx]
            self.rings.append(Ring(self.R-t, self.R, t, 1, ringConfiguration[idx]))

        self.boxes = BoxSet(*boxConfiguration, self.rings[0].final_quaternion)

    
    def __repr__(self):
        # print(self.rings)
        return str(len(self.rings))

    def update(self, updateQuaternion):
        ''''''
        self.quaternion.multiply(updateQuaternion)

        self.i = self.quaternion.rotate(self.unitBasis[0])
        self.j = self.quaternion.rotate(self.unitBasis[1])
        self.k = self.quaternion.rotate(self.unitBasis[2])
        self.basis = np.array([self.i, self.j, self.k])
        
        for idx, ring in enumerate(self.rings):
            ring.update(updateQuaternion)
        
        

    def plot(self, ax, plotBasis = True):
        '''Plot all rings and boxes'''
        for idx, ring in enumerate(self.rings):
            ring.plot(ax, False, color=self.colors[idx])

        self.boxes.plot(ax, False)

        if plotBasis:
            origin = np.array([0, 0, 0])
            ax.quiver(*origin, *self.i, color="r")
            ax.quiver(*origin, *self.j, color="g")
            ax.quiver(*origin, *self.k, color="b")


        ### Save orientation by means of principle axis in global tumbleweed cordinate frame
        ### add point masses to one of the rings (tristan code?)
        ### Compute total inertia tensor of tumbleweed


