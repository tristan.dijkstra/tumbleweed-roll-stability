import makeModel as tmm
import numpy as np
import math

class BoxSet:
    def getSteiner(self, mass, x, y, z):
        '''Takes mass and carthesian coordinates (of a block) and outputs an array with the three steiner terms'''
        # http://www2.eng.cam.ac.uk/~hemh1/gyroscopes/momentinertia.html
        steiner = mass * np.array([y**2 + z**2, x**2 + z**2, x**2 + y**2])
        return steiner

    def massMOI(self, x, y, z):
        '''Finds Mass Moment of Inertia for all blocks'''
        Iarray = []
        # gets the steiner array for each block using the getSteiner method
        for index in range(self.numberOfBoxes):
            Iarray.append(self.getSteiner(self.masses[index], x[index], y[index], z[index]))
        return np.array(Iarray)

    def __init__(self, radius, numberOfBoxes, boxMasses, quaternion, startAngle = 0):
        '''Ring Class that makes up one of the rings on the tumbleweed'''
        assert numberOfBoxes % 2 == 0, "Attribute numberOfBoxes must be an equal number"

        self.r = radius
        self.numberOfBoxes = numberOfBoxes
        self.masses = np.array(boxMasses)
        self.quaternion = quaternion

        self.parentBasis = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])

        self.i = self.quaternion.rotate(self.parentBasis[0])
        self.j = self.quaternion.rotate(self.parentBasis[1])
        self.k = self.quaternion.rotate(self.parentBasis[2])

        angles = [((360/self.numberOfBoxes) * x) + startAngle for x in range(self.numberOfBoxes)]
        x = np.array([self.r * math.cos(math.radians(angle)) for angle in angles])
        y = np.array([self.r * math.sin(math.radians(angle)) for angle in angles])
        z = np.empty(self.numberOfBoxes)
        self.MMOIlist = self.massMOI(x, y, z)
        self.coords = np.array([x, y, z]).T
        self.Io = np.sum(self.MMOIlist, axis=0)


ringRadius = 5
ringMass = 5
numBoxes = 8
boxMasses = [2 for x in range(numBoxes)]
tristan = tmm.qr(45, 1, 0, 0)
ring1 = BoxSet(ringRadius, numBoxes, boxMasses, tristan)

print(ring1.coords)
