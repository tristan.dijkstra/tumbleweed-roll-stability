import numpy as np
import math

class Ring:
    def getSteiner(self, mass, x, y, z):
        '''Takes mass and carthesian coordinates (of a block) and outputs an array with the three steiner terms'''
        # http://www2.eng.cam.ac.uk/~hemh1/gyroscopes/momentinertia.html
        steiner = mass * np.array([y**2 + z**2, x**2 + z**2, x**2 + y**2])
        return steiner

    def massMOI(self, x, y, z):
        '''Finds Mass Moment of Inertia for all blocks'''
        # empty array
        Iarray = np.zeros(self.numberOfBoxes, dtype=object)
        # gets the steiner array for each block using the getSteiner method
        for index in range(self.numberOfBoxes):
            Iarray[index] = self.getSteiner(self.masses[index], x[index], y[index], z[index])
        return Iarray

    def __init__(self, radius, ringMass, numberOfBoxes, boxMasses):
        '''Ring Class that makes up one of the rings on the tumbleweed'''
        self.radius = radius
        self.ringMass = ringMass
        self.numberOfBoxes = numberOfBoxes
        self.masses = np.array(boxMasses)
        self.totalMass = sum(self.masses) + self.ringMass
        self.angles = [(360/self.numberOfBoxes) * x for x in range(self.numberOfBoxes)]

        self.x = np.array([self.radius * math.cos(math.radians(angle)) for angle in self.angles])
        self.y = np.array([self.radius * math.sin(math.radians(angle)) for angle in self.angles])
        self.z = np.empty(self.numberOfBoxes)
        self.yToGround = self.y + radius
        self.MMOIlist = self.massMOI(self.x,self.y,self.z)  
        self.Io = np.sum(self.MMOIlist, axis=0)
        self.groundSteiner = self.getSteiner(self.totalMass, 0, radius, 0)
        self.Iground = self.groundSteiner + self.Io


class Tumbleweed:
    def __init__(self, velocity, radius):
        self.velocity = velocity
        self.radius = radius
        self.angularVelocity = self.velocity/self.radius


ringRadius = 5
ringMass = 5
numBoxes = 8
boxMasses = [2 for x in range(numBoxes)]

ring1 = Ring(ringRadius, numBoxes, ringMass, boxMasses)
velocity = 20 #m/s
print(ring1.Iground)
