import makeModel as tmm
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


worldAxis = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])

# Induvidual ring test
tristan = tmm.qr(45, 1, 0, 0)
ring1 = tmm.Ring(4.95,5,0.1,2900, tristan)

# Configure Rings
ringMass = 5

tumbleweedV3 = [tmm.qr(0, 1,0,0), tmm.qr(60, 1,0,0), tmm.qr(-60, 1,0,0), 
                tmm.qr(30, 1, np.sqrt(3), 0), tmm.qr(-30, 1, np.sqrt(3), 0),
                tmm.qr(30, 1, -np.sqrt(3), 0), tmm.qr(-30, 1, -np.sqrt(3), 0)]
#ringConfig = [tmm.qr(45, 1, 0, 0), tmm.qr(45, 0, 1, 0), tmm.qr(45, 0, 0, 1), tmm.qr(-45, 1, 0, 0), tmm.qr(-45, 0, 1, 0), tmm.qr(-45, 0, 0, 1)]
ringConfig = [tmm.qr(90, 0, 1, 0), tmm.qr(45, 1, 0, 0), tmm.qr(90, 1, 0, 0), tmm.qr(-45, 1, 0, 0), tmm.qr(0, 1, 0, 0)]#, tmm.qr(-45, 1, 0, 0), tmm.qr(-45, 0, 1, 0), tmm.qr(-45, 0, 0, 1)]

ringConfig = tumbleweedV3
tConfig = [0.01 for x in ringConfig]

# Configurate Boxes
ringRadius = 5
numBoxes = 6
boxMasses = [2 for x in range(numBoxes)]

boxConfig = [ringRadius, numBoxes, boxMasses]

# Create Tumbler
OffsetBasis = tmm.qr(0, 0, 0, 1)
tumbler = tmm.Tumbleweed(5, ringConfig, tConfig, boxConfig, OffsetBasis)

# Create plots to display
fig = plt.figure(figsize=plt.figaspect(1) * 2)
ax = fig.add_subplot(111, projection='3d')

# Define Plot axis sizes
minmax = -6, 6


# show plot

rotationIncrement = 1
for i in range(0,480, rotationIncrement):
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlim(minmax)
    ax.set_ylim(minmax)
    ax.set_zlim(minmax)
    
    UpdateRotation = tmm.qr(rotationIncrement, 1, 0, 0)
    tumbler.update(UpdateRotation)
    tumbler.plot(ax)
    plt.pause(0.000001)

    plt.clf()

