import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Aye so this is not bullshit as it turns out
angle = np.array([0, 180]) #from 0 to 90 degrees
rotation = np.array([0,0,1]) #rotation around the y and z axis (i, j, k)
coordinate = np.array([2,0,0])

def QuaternionRotation(angle, i, j, k):
    angle = np.radians(angle/2)
    Quaternion = np.array([np.cos(angle), i * np.sin(angle), j * np.sin(angle), k * np.sin(angle)])
    unitQuaternion = Quaternion / np.linalg.norm(Quaternion)
    r,i,j,k = unitQuaternion[0], unitQuaternion[1], unitQuaternion[2], unitQuaternion[3]
    rotationMatrix = np.array(
        [[1 - (2 * (j**2 + k**2)), 2 * (i * j - k * r), 2 * (i * k + j * r)],
         [2 * (i * j + k * r), 1 - (2 * (i**2 + k**2)), 2 * (j * k - i * r)],
         [2 * (i * k - j * r), 2 * (j * k + i * r), 1 - (2 * (i**2 + j**2))]])
    return rotationMatrix

p = np.array([5,0,0])
a = QuaternionRotation(90, 0, 0, 1)
b = QuaternionRotation(90, 1, 0, 0)
c = np.matmul(b,a)
pnew = np.matmul(c, p)
print(pnew)


x = []
y = []
z = []
angles = np.linspace(0, 180, 10)
angles2 = np.linspace(180, 300, 10)
start = [5,0,0]
for angle in angles:
    newPoint = np.matmul(QuaternionRotation(angle, 0, 0, 1), start)
    x.append(newPoint[0])
    y.append(newPoint[1])
    z.append(newPoint[2])
for angle in angles2:
    newPoint = np.matmul(QuaternionRotation(angle, 0, 1, 0), start)
    x.append(newPoint[0])
    y.append(newPoint[1])
    z.append(newPoint[2])


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
origin = [0], [0], [0]  # origin point
# ax.quiver(*origin, x, y, z)
ax.set_xlim([-5, 5])
ax.set_ylim([-5, 5])
ax.set_zlim([-5, 5])
# ax.quiver(*origin, start[0], start[1], start[2])
ax.plot(x, y, z)
plt.show()
