import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Aye so this is bullshit as it turns out
angle = np.array([0, 45]) #from 0 to 90 degrees
rotation = np.array([0,0,1]) #rotation around the y and z axis (i, j, k)
coordinate = np.array([2,0,0])

def q(angle, normalizedRotation):
    return np.cos(angle) * np.array([1, 0, 0, 0]) + (normalizedRotation * np.sin(angle))

def qInv(angle, normalizedRotation):
    # realInv = np.cos(-angle) + np.sin(-angle)
    # print((normalizedRotation * np.sin(-angle)))
    print(np.cos(-angle) * np.array([1,0,0,0]) + (normalizedRotation * np.sin(-angle)))
    return np.cos(-angle) * np.array([1, 0, 0, 0]) + (normalizedRotation * np.sin(-angle))

x = []
y = []
z = []
def Quaternion(angle, rotation, coordinate):
    angle = np.radians(angle/2)
    normalizedRotation = (rotation / np.linalg.norm(rotation))
    normalizedRotation = np.append([0], normalizedRotation)
    angles = np.linspace(angle[0], angle[1], 10)
    # print(np.degrees(angles))
    for angle in angles:
        qq = q(angle, normalizedRotation)
        qqInv = qInv(angle, normalizedRotation)
        result = qq * np.append([0], coordinate) * qqInv

        print(result)
        x.append(result[1])
        y.append(result[2])
        z.append(result[3])
Quaternion(angle, rotation, coordinate)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
origin = [0], [0], [0]  # origin point
ax.quiver(*origin, x, y, z)
ax.set_xlim([-5, 5])
ax.set_ylim([-5, 5])
ax.set_zlim([-5, 5])
ax.quiver(*origin, coordinate[0], coordinate[1], coordinate[2])
# ax.plot(x, y, z)
plt.show()
