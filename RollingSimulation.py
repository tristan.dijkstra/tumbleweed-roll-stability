import generateTumbleweed as gen
import simTools as st

import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits import mplot3d
from matplotlib import cm


# -------------------------------------------------------------------
# Global Variables
# -------------------------------------------------------------------
''' 
Y-direction is the ground-sky direction (flat rolling on a const-y surface) 
Wind in positive x direction
'''

world_basis = np.array([[1,0,0],[0,1,0],[0,0,1]])

# -------------------------------------------------------------------
# Initite Model
# -------------------------------------------------------------------

# Model parameters
r0          = 2.5       # [m]
Cpi         = 0.1       # [-]
N_twist_tot = 2.0      # [-]
N_helix = 3             # [-]

# Initiation
Tumbler = gen.Polyhelical(world_basis, r0, Cpi, N_twist_tot, N_helix, 16)

# -------------------------------------------------------------------
# Initite Physics Simulation
# -------------------------------------------------------------------

# Simulation parameters
t_init = 0
t_end = 20
dt = 0.0001

wind_speed      = 5
wind_direction  = 0


# Initiation
state = {
    "v"         : np.array([0,0,0]),
    "w"         : np.array([0,0,0]),
    "p"         : np.array([0,0,0]),
    "grnd_vec"  : np.array([0,0,0]),
    "h"         : Tumbler.shape[np.argmin(Tumbler.shape, axis=0)[1]],
    "dh"        : np.array([0,0,0]),
    "basis"     : Tumbler.basis,
    "wind"      : 0,
    "acc"       : 0}   

ttab = []
vtab = []
wtab = []
ptab = []
btab = []
#windtab = []
atab = []
htab = []

# -------------------------------------------------------------------
# Physics Time-step Simulation
# -------------------------------------------------------------------

t = t_init
print("/// COMMENCE ROLLING PROCDURE ///")

while t <= t_end:
    ttab.append(t)
    vtab.append(np.linalg.norm(state["v"]))
    ptab.append(state["p"])
    btab.append(state["basis"])
    wtab.append(state["w"])
    atab.append(np.linalg.norm(state["acc"]))
    htab.append(state["h"][1])

    state = st.blowdown(Tumbler, wind_speed, wind_direction, dt)

    t += dt

    #TODO : Progress bar

print("/// ROLLING TERMINATED ///")

ttab = np.array(ttab)
ptab = np.delete(np.array(ptab),1,1)
vtab = np.array(vtab)
btab = np.array(btab)
wtab = np.array(wtab)

plt.plot(*ptab.T)
plt.axis('equal')
plt.show() 

plt.plot(ttab, vtab)
plt.show() 

st.replay(Tumbler, btab[::10])
