import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits import mplot3d


# -------------------------------------------------------------------
# Global Variables
# -------------------------------------------------------------------
unitBasis = np.array([[1,0,0],[0,1,0],[0,0,1]])
model_resolution = 0.2


# -------------------------------------------------------------------
# Coordinate Transformation Functions (CTFs)
# -------------------------------------------------------------------
def c_to_r(R):
    '''Transforms a array of vectors [[r,theta,z]] in cylindrical coordinates to a array of vectors [[x,y,z]] in cartisian coordinates'''
    r, theta, z = R.T
    return np.array([
        r * np.cos(theta),
        r * np.sin(theta),
        z
    ]).T


def r_to_c(P):
    '''Transforms a array of vectors [[x,y,z]] in cartisian coordinates to a array of vectors [[r,theta,z]] in cylindrical coordinates'''
    x, y, z = P.T
    return np.array([
        np.sqrt(x**2 + y**2),
        np.arctan2(y, x),
        z
    ]).T


# -------------------------------------------------------------------
# Shape Class Definitions 
# -------------------------------------------------------------------
class Shape:
    def __init__(self, basis):
        '''
        The parent Shape object contains a basis property that can be rotated.
        
        Parameters:
            basis (ndarray): 3x3 shape basis, matrix of i,j,k vectors.

        Attributes:
            basis (ndarray): 3x3 shape basis, matrix of i,j,k vectors.
        '''
        
        self.basis = basis


    def update_basis(self, quat_update):
        '''
        Update the basis of a shape based on a quaternion rotation.

        Parameters:
            quat_update (obj) -- QuaternionRotation object [-]
        '''

        self.basis = quat_update.rotate(self.basis)
        
        

class Helix_hemisphere(Shape):
    def __init__(self, basis, r0, Cpi, z_offset, N_rot):
        '''
        Hemisphere Helical shape object.
        
        Parameters:
            basis (ndarray) -- 3x3 shape basis, matrix of i,j,k vectors [-]
            r0 (float) -- Radius of hemisphere [m]
            Cpi (float) -- Pill factor of shape (not used, added for consistency with other classes) [-]
            z_offset (float) -- Offset of shape from origin [m]
            N_rot (float) -- Rotation angle of helix over the length [rad]
        
        Attributes:
            basis (ndarray) -- 3x3 shape basis, matrix of i,j,k vectors [-]
            r0 (float) -- Radius of hemisphere [m]
            Cpi (float) -- Pill factor of shape (not used, added for consistency with other classes) [-]
            z_offset (float) -- Offset of shape from origin [m]
            N_rot (float) -- Rotation angle of helix over the length [rad]
            vertices_c (ndarray) -- Array of points on helix in cylindrical coordinates [m]
            vertices (ndarray) -- Array of points on helix in cartisian coordinates [m]
        '''
        
        super().__init__(basis)
        self.r0 = r0
        self.Cpi = Cpi
        self.z_offset = z_offset
        self.N_rot = N_rot

        self.shape_points()
    

    def r(self,t):
        '''
        Radius function of hemispherical helix.

        Parameters:
            t (ndarray) -- array of parameter values at which to evaluate radius [-]

        Returns:
            r (ndarray) -- array with radius at every value given in t [m]
        '''
        
        return np.sqrt(self.r0**2 - self.r0**2 * t**2)
    

    def theta(self,t):
        '''
        Rotation angle function of hemispherical helix.

        Parameters:
            t (ndarray) -- array of parameter values at which to evaluate radius [-]

        Returns:
            theta (ndarray) -- array with angle theta at every value given in t [rad]
        '''

        return 2*np.pi * self.N_rot * t


    def z(self,t):
        '''
        z-position function of hemispherical helix.

        Parameters:
            t (ndarray) -- array of parameter values at which to evaluate radius [-]

        Returns:
            z (ndarray) -- array with z-position at every value given in t [m]
        '''

        return self.r0 * t + self.z_offset


    def eval(self, t):
        '''
        Hemispherical helix position function.

        Parameters:
            t (ndarray) -- array of parameter values at which to evaluate radius [-]

        Returns:
            R (ndarray) -- array with r,theta,z position values given at every value in t [m]
        '''

        return np.array([
            np.sqrt(self.r0**2 - self.r0**2 * t**2),
            2*np.pi * self.N_rot * t,
            self.r0 * t + self.z_offset                               
        ]).T


    def eval_dot(self,t):
        '''
        Hemispherical helix velocity function.

        Parameters:
            t (ndarray) -- array of parameter values at which to evaluate radius [-]

        Returns:
            R (ndarray) -- array with r,theta,z velocity values given at every value in t [m/-]
        '''

        return np.array([
            -self.r0**2*t / (np.sqrt(self.r0**2 - self.r0**2 * t**2)),
            2*np.pi * self.N_rot * np.sqrt(self.r0**2 - self.r0**2 * t**2),
            self.r0                               
        ]).T


    def gen_spacing(self, ds=0.05):
        '''
        Generate parameter values for equidistant spacing of positions.

        Parameters:
            ds (float) -- linear distance between points [m]

        Returns:
            T (ndarray) -- array with parameter values resulting in equidistant spacing along shape [-]
        '''

        T = []
        t = 0
        while t < 1:
            T.append(t)        
            v = np.linalg.norm(self.eval_dot(t))
            dt = ds/v
            
            t = t+dt
        
        a = np.linalg.norm(self.eval(T[-2]) - self.eval(T[-1]))
        b = np.linalg.norm(self.eval(T[-1]) - self.eval(1))

        if b/a > 0.5:
            T.append(1)
        else:
            T[-1] = 1

        return np.array(T)


    def shape_points(self, dist_step=model_resolution):
        '''
        Generate list of equidistant points on helix. Called once upon creation of shape.

        Parameters:
            dist_step (float) -- linear distance between points [m]
        '''

        T = self.gen_spacing(dist_step)
        self.vertices_c = self.eval(T)

        self.vertices = np.zeros((len(self.vertices_c),3))
        for i in range(len(self.vertices_c)):
            self.vertices[i] = c_to_r(self.vertices_c[i]).dot(self.basis)

        plt.plot
        


class Helix_cylinder(Shape):
    def __init__(self, basis, r0, Cpi,z_offset, N_rot):
        '''
        Cylindrical Helical shape object.
        
        Parameters:
            basis (ndarray) -- 3x3 shape basis, matrix of i,j,k vectors [-]
            r0 (float) -- Radius of cylinder [m]
            Cpi (float) -- Pill factor of model [-]
            z_offset (float) -- Offset of shape from origin [m]
            N_rot (float) -- Rotation angle of helix over the length [rad]
        
        Attributes:
            basis (ndarray) -- 3x3 shape basis, matrix of i,j,k vectors [-]
            r0 (float) -- Radius of hemisphere [m]
            Cpi (float) -- Pill factor of shape (not used, added for consistency with other classes) [-]
            z_offset (float) -- Offset of shape from origin [m]
            N_rot (float) -- Rotation angle of helix over the length [rad]
            vertices_c (ndarray) -- Array of points on helix in cylindrical coordinates [m]
            vertices (ndarray) -- Array of points on helix in cartisian coordinates [m]
        '''

        super().__init__(basis)
        self.r0 = r0
        self.Cpi = Cpi
        self.z_offset = z_offset
        self.N_rot = N_rot

        self.shape_points()


    def gen_spacing(self, ds=0.05):
        '''
        Generate parameter values for equidistant spacing of positions.

        Parameters:
            ds (float) -- linear distance between points [m]

        Returns:
            T (ndarray) -- array with parameter values resulting in equidistant spacing along shape [-]
        '''
        
        T = []
        t = 0
        while t < 1:
            T.append(t)        
            v = np.linalg.norm(self.eval_dot(t))
            dt = ds/v
            
            t = t+dt
        if len(T) > 2 :
            a = np.linalg.norm(self.eval(T[-2]) - self.eval(T[-1]))
            b = np.linalg.norm(self.eval(T[-1]) - self.eval(1))

            if b/a > 0.5:
                T.append(1)
            else:
                T[-1] = 1

        return np.array(T)


    def eval(self, t):
        '''
        Cylindrical helix position function.

        Parameters:
            t (ndarray) -- array of parameter values at which to evaluate radius [-]

        Returns:
            R (ndarray) -- array with r,theta,z position values given at every value in t [m]
        '''

        return np.array([
            self.r0 + (t-t),
            2*np.pi * self.N_rot * t,
            2*self.r0*self.Cpi * t + self.z_offset                               
        ]).T


    def eval_dot(self,t):
        '''
        Cylindrical helix velocity function.

        Parameters:
            t (ndarray) -- array of parameter values at which to evaluate radius [-]

        Returns:
            R (ndarray) -- array with r,theta,z velocity values given at every value in t [m/-]
        '''

        return np.array([
            0,
            2*np.pi * self.N_rot * self.r0,
            2*self.r0*self.Cpi                               
        ]).T


    def shape_points(self, dist_step=model_resolution):
        '''
        Generate list of equidistant points on helix. Called once upon creation of shape.

        Parameters:
            dist_step (float) -- linear distance between points [m]
        '''

        T = self.gen_spacing(dist_step)
        self.vertices_c = self.eval(T)

        self.vertices = np.zeros((len(self.vertices_c),3))
        for i in range(len(self.vertices_c)):
            self.vertices[i] = c_to_r(self.vertices_c[i]).dot(self.basis)


# -------------------------------------------------------------------
# Polyhelical Class Definitions 
# -------------------------------------------------------------------
class Polyhelical(Shape):
    def __init__(self, basis, r0,Cpi, N_twist_tot, N_helix, M_tot):
        '''
        Polyhelical Design Shape class with constant twist density

        Parameters:
            basis (ndarray) -- 3x3 shape basis, matrix of i,j,k vectors.
            r0 (float) -- Radius of hemisphere [m]
            Cpi (float) -- Pill factor of shape (not used, added for consistency with other classes) [-]
            N_twist_tot (float) -- Total Twist angle of a single arc [-]
            N_helix (int) -- Helix count in model [-]
            M_tot (float) -- Total mass of the structure [kg]

        Attributes:
            r0 (float) -- Radius of hemisphere [m]
            Cpi (float) -- Pill factor of shape (not used, added for consistency with other classes) [-]
            N_twist_tot (float) -- Total Twist angle of a single arc [-]
            N_helix (int) -- Helix count in model [-]
            M_tot (float) -- Total mass of the structure [kg]
            
            d_maj (float) -- Major diameter [m]
            d_min (float) -- Minor diameter [m]
            Cd (float) -- Coefficient of drag [-]

            N_twist_cylinder (float) -- Twist angle in cylindrical section [rad]
            N_twist_hemisphere (float) -- Twist angle in hemisphere sections section [rad]
            
            helices (array) -- Array of helix quaturnion rotation objects [-]
            shapes (ndarray) -- Configuration of the model containing all shape sections [-]
            geometry (ndarray) -- Fixed array of points forming the original shape of the model [-]
            shape (ndarray) -- Changing array of points forming the current shape based on the orientation of the model [-]
            
            M_vertex (float)
            Ixx (float) -- Mass Moment of Inertia around the x-axis [kg*m^2]
            Iyy (float) -- Mass Moment of Inertia around the y-axis [kg*m^2]
            Izz (float) -- Mass Moment of Inertia around the z-axis [kg*m^2]
            Ixy (float) -- Product Mass Moment of Inertia relative to the x,y-axis [kg*m^2]
            Ixz (float) -- Product Mass Moment of Inertia relative to the x,z-axis [kg*m^2]
            Iyz (float) -- Product Mass Moment of Inertia relative to the y,z-axis [kg*m^2]

            I_master (ndarray) -- Fixed intertia tensor of the origional shape [-]
            I (ndarray) -- Changing intertia tensor of the current shape based on the orientation of the model [-]
        '''

        #Initiate given Parameters as Attributes
        super().__init__(basis)
        self.r0 = r0
        self.Cpi = Cpi
        self.N_twist_tot = N_twist_tot
        self.N_helix = N_helix
        self.M_tot = M_tot

        # Computer Rover Properties
        self.d_maj = 2*self.r0*(1+self.Cpi)
        self.d_min = 2*self.r0

        self.N_twist_cylinder    = self.N_twist_tot / self.d_maj * 2*self.r0*self.Cpi
        self.N_twist_hemisphere  = self.N_twist_tot / self.d_maj * self.r0

        # Define sub-Polyhelical quaturnions
        spacing = np.linspace(0,2*np.pi,self.N_helix, False)
        self.helices = []
        for i in range(len(spacing)):

            self.helices.append(QuaternionRotation(spacing[i], np.array([0,0,1])))

        self.Cd = 2 #conservative

        # Define sub-helix quaturnions
        quat_neg_z = QuaternionRotation(np.pi, np.array([0,1,0]))
        quat_cyl_offset = QuaternionRotation(np.pi, np.array([0,0,1]))
        quat_hem2_offset = QuaternionRotation(np.pi*(1-2*self.N_twist_cylinder), np.array([0,0,1]))

        config = []
        for this_helix in self.helices:
            helix_basis = this_helix.rotate(np.array([[1,0,0],[0,1,0],[0,0,1]]))
            inv_helix_basis = quat_neg_z.rotate(helix_basis)
            config.append(Helix_hemisphere(helix_basis,    self.r0,     self.Cpi, self.r0*self.Cpi, self.N_twist_hemisphere))
            config.append(Helix_hemisphere(quat_hem2_offset.rotate(inv_helix_basis),  self.r0,     self.Cpi, self.r0*self.Cpi, self.N_twist_hemisphere))
            #config.append(Helix_hemisphere(inv_helix_basis,  self.r0,     self.Cpi, self.r0*self.Cpi, self.N_twist_hemisphere))
            config.append(Helix_cylinder(quat_cyl_offset.rotate(inv_helix_basis),    self.r0,     self.Cpi, -self.r0*self.Cpi, self.N_twist_cylinder))

        # Define all points relative to Polyhelical instead of subshapes
        self.shapes = config
        geometry = []
        for shape in self.shapes:
            for vertex in shape.vertices:
                geometry.append(vertex)
             
        self.geometry = np.array(geometry)
        self.shape = self.geometry

        # Inertia Calculation
        self.M_vertex = self.M_tot / len(self.shape)
        x,y,z = self.shape.T
        self.Ixx = sum(y**2 + z**2) * self.M_vertex
        self.Iyy = sum(x**2 + z**2) * self.M_vertex
        self.Izz = sum(x**2 + y**2) * self.M_vertex

        self.Ixy = sum(x * y)       * self.M_vertex
        self.Ixz = sum(x * z)       * self.M_vertex
        self.Iyz = sum(y * z)       * self.M_vertex
        
        self.I_master = np.array([
            [ self.Ixx, -self.Ixy, -self.Ixz],
            [-self.Ixy,  self.Iyy, -self.Iyz],
            [-self.Ixz, -self.Iyz,  self.Izz]])

        self.I = np.copy(self.I_master)

        # Dynamic inital variables
        
        self.ang_velocity = np.array([0,0,0])
        self.velocity = np.array([0,0,0])
        self.position = np.array([0,0,0])

        
    def plot(self, ax):
        '''
        Plots the current shape of the model.

        Parameters:
            ax (obj) -- matplotlib axis construct to plot on [-]
        '''
        ax.plot(*self.shape.T,"ok", markersize=0.3)
        

    def update(self,quat_update):
        '''
        Update the current orientation of the model based on a quaturnion rotation.

        Parameters:
            quat_update (obj) -- Quaturnion Rotation object [-]
        '''
        self.basis = quat_update.rotate(self.basis)
        self.shape = self.geometry.dot(self.basis)

        # I_master is known around self.basis (does not change w.r.t body axes)
        i,j,k = unitBasis  
        q,r,s = self.basis
        T = np.array([[np.dot(i,q), np.dot(i,r), np.dot(i,s)],
                  [np.dot(j,q), np.dot(j,r), np.dot(j,s)],
                  [np.dot(k,q), np.dot(k,r), np.dot(k,s)]])
    
        self.I = np.matmul(np.matmul(T, self.I_master), np.transpose(T))


    def sectionArea(self,axis):
        '''
        Calculates the maximum cross sectional area on the plane normal to the direction

        Parameters:
            axis (ndarray) -- axis in 3D space describing the direction along which the cross section area is to be known [-]

        Returns:
            area (float) -- cross sectional area [m]
        '''

        z_vec = self.basis[2]
        denom = np.linalg.norm(axis) * np.linalg.norm(z_vec)

        if denom != 0:
            relative_wind_cos = np.dot(axis, z_vec) / denom
        else:
            relative_wind_cos = 1

        return self.r0**2 * self.Cpi * relative_wind_cos + np.pi * self.r0**2


# -------------------------------------------------------------------
# Quaturnion Rotation Class Definitions
# -------------------------------------------------------------------
class QuaternionRotation:
    def __init__(self, angle, axis):
        '''
        Rotate Quaternion object defining a rotation in 3D space.
        
        Parameters:
            angle (float) -- Angle ro be rotated by [rad]
            axis (ndarray) -- vector describing rotation axis
        
        Attributes:
            angle (float) -- Angle used for quaternion rotation, half actual angle [rad]
            Quaternion (ndarray) -- 4D quaternion vector theta,i,j,k [-]
            unitQuaternion (ndarray) -- Normalized quaturnion vector [-]
            rotationMatrix (ndarray) -- 3x3 rotation matrix describing defined rotation[-] 
        '''

        i,j,k = axis.T
        self.angle = angle/2
        self.Quaternion = np.array(
            [np.cos(self.angle), i * np.sin(self.angle), j * np.sin(self.angle), k * np.sin(self.angle)])
        self.refresh()


    def refresh(self, newAngle=False):
        '''
        Rates unit quaternion and rotation matrix from quaternion
        
        Parameters:
            newAngle (Bool) -- Flag if angle should be updated
        '''
        self.unitQuaternion = self.Quaternion / np.linalg.norm(self.Quaternion)
        r, i, j, k = self.unitQuaternion[0], self.unitQuaternion[1], self.unitQuaternion[2], self.unitQuaternion[3]
        
        if newAngle:
            self.angle = np.arccos(r)

        # Formula 2.6 from:
        # https://www.sciencedirect.com/topics/computer-science/quaternion-multiplication
        self.rotationMatrix = np.array(
            [[1 - (2 * (j**2 + k**2)), 2 * (i * j - k * r), 2 * (i * k + j * r)],
             [2 * (i * j + k * r), 1 - (2 * (i**2 + k**2)), 2 * (j * k - i * r)],
             [2 * (i * k - j * r), 2 * (j * k + i * r), 1 - (2 * (i**2 + j**2))]])


    def __repr__(self):
        ''' '''
        # might remove this or change to return unit quaternion
        return self.rotationMatrix

    def __str__(self):
        '''Returns unit Quaternion'''
        return np.array2string(self.unitQuaternion) 

    def rotate(self, coordinate):
        '''Rotate a vector coordinate takes vector to be rotated as input'''
        result = np.zeros((len(coordinate),3))
        
        for i, coord in enumerate(coordinate):
            result[i] = (np.matmul(self.rotationMatrix, coord))
        
        return result
        

    def multiply(self, q2):
        '''Add an aditional compound a rotation by means of quaternion multiplication. Takes q2 as a quaternion vector'''
        xyz1, xyz2 = self.Quaternion[1:4], q2.Quaternion[1:4]   #imaginary values of quaternions
        r1, r2 = self.Quaternion[0], q2.Quaternion[0]           #real value of quaternions
        
        # Formula 2.5 from:
        # https://www.sciencedirect.com/topics/computer-science/quaternion-multiplication
        xyznew = np.cross(xyz2, xyz1) + r2 * xyz1 + r1 * xyz2
        rnew = r2 * r1 - np.dot(xyz2,xyz1)
        
        qnew = [rnew, xyznew[0], xyznew[1], xyznew[2]]
        self.Quaternion = np.array(qnew)
        self.refresh(newAngle=True)

