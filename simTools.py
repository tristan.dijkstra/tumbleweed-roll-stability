import generateTumbleweed as gen

import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits import mplot3d
from matplotlib import cm


# -------------------------------------------------------------------
# Global Variables
# -------------------------------------------------------------------

world_basis = np.array([[1,0,0],[0,1,0],[0,0,1]])

prefix = {
    1:"Mono", 
    2:"Di",
    3:"Tri",
    4:"Tetra",
    5:"Penta",
    6:"Hexa",
    7:"Hepta",
    8:"Octa",
    9:"Ennea",
    10:"Deca"
    }

#Earth Data
atm_density = 1.225 #0.07e5
g           = 9.81
# -------------------------------------------------------------------
# Initite Model
# -------------------------------------------------------------------


# Model parameters
r0          = 2.5       # [m]
Cpi         = 0       # [-]
N_twist_tot = 1         # [-]
N_helix = 3             # [-]

# Initiation
Tumbler = gen.Polyhelical(world_basis, r0, Cpi, N_twist_tot, N_helix, 16)


# -------------------------------------------------------------------
# Simulation Tool functions
# -------------------------------------------------------------------

def show_model():
    fig = plt.figure(figsize=plt.figaspect(1) * 2)
    ax = fig.add_subplot(111, projection='3d')
    
    ax.set_title("%sHelical-T%.2f" % (prefix.get(N_helix, "Poly"), N_twist_tot))
    ax.set_xlim(-4,4)
    ax.set_ylim(-4,4)
    ax.set_zlim(-4,4)

    Tumbler.plot(ax)

    origin = np.array([0, 0, 0])
    #ax.quiver(*origin, *Tumbler.basis[0], color="r")
    #ax.quiver(*origin, *Tumbler.basis[1], color="g")
    ax.quiver(*origin, *Tumbler.basis[2], color="b")

    plt.show()

def flatspot_analysis(steps=360):
    
    quat_maj_roll = gen.QuaternionRotation(2*np.pi/steps, np.array([1,0,0]))
    rot_axis = world_basis

    xtab = [] # minor rolling direction
    ytab = [] # major rolling direction
    ztab = [] # Height of center of mass

    for maj_roll_dir in np.linspace(0,2*np.pi,steps):
        x = []
        y = []
        z = []

        quat_min_roll = gen.QuaternionRotation(np.degrees(2*np.pi/steps), rot_axis[2])
        
        for min_roll_dir in np.linspace(0,2*np.pi,steps):
            x.append(min_roll_dir)
            y.append(maj_roll_dir)
            z.append(-np.min(Tumbler.shape.T[1]))
            #show_model()

            Tumbler.update(quat_min_roll)
            
            

        #Add current to total tabular
        xtab.append(np.array(x))
        ytab.append(np.array(y))
        ztab.append(np.array(z))
        
        rot_axis = quat_maj_roll.rotate(rot_axis)
        Tumbler.update(quat_maj_roll)
        

    return np.array([xtab,ytab,ztab])

def blowdown(model, wind_vel, wind_angle, dt = 0.01, g=9.81):
    
    '''
    wind_vec    = np.array([wind_vel * np.cos(wind_angle), 0, wind_vel * np.sin(wind_angle)])
    try:
        relative_wind_cos = np.dot(wind_vec, z_vec) / (np.linalg.norm(wind_vec) * np.linalg.norm(z_vec))
    except:
        relative_wind_cos = 1

    vel_proj = wind_vec #np.dot(wind_vec, model.velocity)/(np.linalg.norm(wind_vec)**2)*wind_vec
    vel_mag_relative = np.linalg.norm(wind_vec - model.velocity) #FIXME: you do actually have to project here. we need relative velocity in the direction of the wind

    
    F_wind_mag = 
    '''
    abs_wind_vec    = np.array([wind_vel * np.cos(wind_angle), 0, wind_vel * np.sin(wind_angle)])
    rel_wind_vec    = abs_wind_vec - model.velocity
    rel_wind_mag    = np.linalg.norm(rel_wind_vec)

    Fw = 1/2 * atm_density * rel_wind_mag**2 * model.Cd * model.sectionArea(rel_wind_vec) * (rel_wind_vec/rel_wind_mag)

    # Resulting Moment Calculation
    F_mu = -Fw
    F_n = np.array([0, model.M_tot * g, 0])
    
    pos_F_contact = model.shape[np.argmin(model.shape, axis=0)[1]] #FIXME: This is not this easy, if model is on a flat fase, the center of the fase must be selected

    M_mu =  np.cross(pos_F_contact, F_mu)
    M_norm = np.cross(pos_F_contact, F_n)
    M_total = M_mu + M_norm

    a = np.matmul(np.linalg.inv(model.I), M_total)

    model.ang_velocity = model.ang_velocity + a*dt
    rotation = model.ang_velocity * dt
    
    rotation_magnitude = np.linalg.norm(rotation)
    rotation_axis = rotation/rotation_magnitude

    
    quat_update = gen.QuaternionRotation(rotation_magnitude, rotation_axis)
    model.update(quat_update)

    updated_contact = quat_update.rotate([pos_F_contact])[0]
    translation_vec = updated_contact - pos_F_contact

    groundtrack_vec = translation_vec * np.array([1,0,1])
    height_change = translation_vec[1]

    model.position = model.position + groundtrack_vec
    model.velocity = np.linalg.norm(groundtrack_vec) / dt

    sim_state = {
        "v"         : model.velocity,
        "w"         : model.ang_velocity,
        "p"         : model.position,
        "grnd_vec"  : groundtrack_vec,
        "h"         : model.shape[np.argmin(model.shape, axis=0)[1]],
        "dh"        : height_change,
        "basis"     : model.basis,
        "wind"      : Fw,
        "acc"       : a
    }   

    return sim_state

def replay(model, btab):
    fig = plt.figure(figsize=plt.figaspect(1) * 2)
    minmax = -10, 10
    
    for state in btab:
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlim(minmax)
        ax.set_ylim(minmax)
        ax.set_zlim(minmax)
        model.basis = state
        model.shape = model.geometry.dot(model.basis)

        model.plot(ax)
        plt.pause(0.00001)
        plt.clf()

def spin(model):
    fig = plt.figure(figsize=plt.figaspect(1) * 2)
    minmax = -10, 10
    rotation = gen.QuaternionRotation(np.pi/180, np.array([0,0,1]))
    
    for i in range(0,360):
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlim(minmax)
        ax.set_ylim(minmax)
        ax.set_zlim(minmax)
        model.update(rotation)

        model.plot(ax)
        plt.pause(0.001)
        plt.clf()
    
# -------------------------------------------------------------------
# Setup Simulation settings
# -------------------------------------------------------------------

if __name__ == "__main__":
    do_show_model = False
    do_flatspot_analysis = False
    do_spin = True
    if do_spin:
        spin(Tumbler)

    if do_show_model:
        show_model()

    if do_flatspot_analysis:
        
        '''
        fig = plt.figure(figsize=plt.figaspect(1) * 2)
        ax = fig.add_subplot(111, projection='3d')

        ax.set_xlim(0,2*np.pi)
        ax.set_ylim(0,2*np.pi)
        #ax.set_zlim(2.4,2.8)

        ax.set_title("%sHelical-T%.2f" % (prefix.get(N_helix, "Poly"), N_twist_tot))
        ax.set_xlabel("Major Axis Rotation")
        ax.set_ylabel("Minor Axis Rotation")
        ax.set_zlabel("Height of CoM")
        '''
        figure_data = flatspot_analysis()
        gradient_data = np.array([figure_data[0], figure_data[1], np.gradient(figure_data[2])[0]])
        curvature_data = np.array([figure_data[0], figure_data[1], np.gradient(gradient_data[2])[0]])
        

        '''
        ax.plot_surface(*figure_data, cmap=cm.coolwarm)
        plt.pause(0.1)

        fig = plt.figure(figsize=plt.figaspect(1) * 2)
        ax = fig.add_subplot(111, projection='3d')

        ax.set_title("%sHelical-T%.2f" % (prefix.get(N_helix, "Poly"), N_twist_tot))
        ax.set_xlabel("Major Axis Rotation")
        ax.set_ylabel("Minor Axis Rotation")
        ax.set_zlabel("Gradient of CoM")
        ax.plot_surface(*gradient_data, cmap=cm.coolwarm)
        plt.show()
        '''

        fig,ax=plt.subplots()
        ax.pcolor(*gradient_data)
        plt.show()

        fig,ax=plt.subplots()
        ax.pcolor(*curvature_data)
        plt.show()
        

        '''
        plt.plot(figure_data[0][0], figure_data[2][0])
        plt.title("%sHelical-T%.2f" % (prefix.get(N_helix, "Poly"), N_twist_tot))
        plt.xlabel("Major Axis Rotation")
        plt.ylabel("Height of CoM")
        plt.ylim(min(2.4,min(figure_data[2][0])) ,2.55)
        plt.show()
        '''

        '''
        for i, min_orient in enumerate(figure_data[0]):
            plt.clf()

            plt.plot(figure_data[0][i], figure_data[2][i])
            plt.title("%sHelical-T%.2f at %s" % (prefix.get(N_helix, "Poly"), N_twist_tot, i*3))
            plt.xlabel("Major Axis Rotation")
            plt.ylabel("Height of CoM")
            plt.ylim(2.2 ,3.1)
            plt.savefig("img/%sHelical-T%.2f at %s.jpg" % (prefix.get(N_helix, "Poly"), N_twist_tot, i*3))
        '''






